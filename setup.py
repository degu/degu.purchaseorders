from setuptools import setup, find_packages
import os

version = '0.1.8'

setup(name='degu.purchaseorders',
      version=version,
      description="Purchase order managment",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Daniel Hernandez',
      author_email='daniel@degu.cl',
      url='https://bitbucket.org/degu/degu.purchaseorders/',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['degu'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'Products.CMFPlone',
          'plone.app.dexterity [grok]',
          'plone.app.referenceablebehavior',
          'plone.app.relationfield',
          'plone.namedfile [blobs]', # makes sure we get blob support
          'archetypes.schemaextender',
          'plone.app.registry',
          'plone.principalsource',
      ],
      entry_points="""
      # -*- Entry points: -*-
      [z3c.autoinclude.plugin]
      target = plone
      """,
      setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],
      )
