# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.directives import form

from zope.interface import Invalid

from plone.namedfile.interfaces import IImageScaleTraversable
from plone.namedfile.field import NamedBlobImage

from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from plone.app.textfield import RichText
from plone.dexterity.content import Container

from degu.purchaseorders import DeguPurchaseOrdersMessageFactory as _

from z3c.relationfield.schema import RelationChoice, RelationList
from plone.formwidget.autocomplete import AutocompleteMultiFieldWidget
from Products.CMFCore.utils import getToolByName

class IPurchaseOrderFolder(form.Schema):
    """A folder to store purchase orders.
    """

    orderClassifications = schema.List(
        title=_(u"Clasificaciones"),
        description=_(u"Las clasificaciones te permiten organizar las órdenes de compra. Ingresa el nombre de una clasificación por cada línea de texto."),
        required=True,
        value_type=schema.TextLine()
    )

    # recipientGroup = schema.Choice(
    #     title=_(u"Grupo destinatario"),
    #     description=_(u"Ingrese un grupo de usuarios que será notificados por email de las órdenes de compra recibidas."),
    #     vocabulary=u"plone.principalsource.Groups",
    #     required=False,
    # )

    form.widget(recipientUsers=AutocompleteMultiFieldWidget)
    recipientUsers = schema.List(
        title=_(u"Destinatarios"),
        description=_(u"Ingrese los usuarios que seran notificados por email de las órdenes de compra recibidas."),
        default=[],
        value_type=schema.Choice(vocabulary=u"plone.principalsource.Users",),           
        required=False,
    )

    # emailRecipients = schema.TextLine(
    #     title=_(u"Destinatarios"),
    #     description=_(u"Emails para notificar las ordenes de compra recibidas. Si son más de uno se deben separar con comas."),
    # )

class PurchaseOrderFolder(Container):
    """The object for purchase order folders
    """

    def emailRecipients(self):
        emails = []
        
        # Append emails in group
        # gtool = getToolByName(self, 'portal_groups')
        # group = gtool.getGroupById('Reviewers')
        # if group != None:
        #     for member in group.getGroupMembers():
        #         email = member.getProperty('email')
        #         if email not in emails:
        #             emails.append(email)

        # Append emails for users
        mtool = getToolByName(self, 'portal_membership')
        for user in self.recipientUsers:
            if mtool.getMemberInfo(user)['has_email']:
                member = mtool.getMemberById(user)
                email = member.getProperty('email')
                if email not in emails:
                    emails.append(email)

        return ", ".join(emails)

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in purchaseorderfolder_templates/view.pt.
    """
    
    grok.context(IPurchaseOrderFolder)
    grok.require('zope2.View')
    grok.name('view')
