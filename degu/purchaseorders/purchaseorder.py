# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.directives import form

from zope.interface import Invalid

from plone.namedfile.interfaces import IImageScaleTraversable
from plone.namedfile.field import NamedBlobImage

from zope.schema.interfaces import IContextSourceBinder
from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from Products.CMFCore.utils import getToolByName
from plone.app.textfield import RichText

from zope.app.container.interfaces import IObjectAddedEvent

from plone.dexterity.content import Item

import logging
logger = logging.getLogger("Plone")


from degu.purchaseorders import DeguPurchaseOrdersMessageFactory as _

@grok.provider(IContextSourceBinder)
def possibleOrderClassifications(context):
    terms = []

    for classification in context.orderClassifications:
        terms.append(SimpleVocabulary.createTerm(classification, classification, classification))

    return SimpleVocabulary(terms)

class IPurchaseOrder(form.Schema):
    """A purchase order.
    """

    classification = schema.Choice(
        title=_(u"Classificación"),
        source=possibleOrderClassifications,
        required=False,
    )

    items = schema.List(
        title=_(u"Productos o servicios"),
        description=_(u"Ingresa un elemento por cada línea de texto."),
        required=True,
        value_type=schema.TextLine()
    )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in purchaseorder_templates/view.pt.
    """
    
    grok.context(IPurchaseOrder)
    grok.require('zope2.View')
    grok.name('view')

class PurchaseOrder(Item):
    """The object for purchase orders
    """

from smtplib import SMTPRecipientsRefused
import datetime
import time
from email import utils

@grok.subscribe(IPurchaseOrder, IObjectAddedEvent)
def afterPurchaseOrderCreated(purchaseOrder, event):
    logger.info('Created a Purchase Order')

    if purchaseOrder.emailRecipients() != '':
        # Set the time when order is created
        emailDate = utils.formatdate(time.mktime(datetime.datetime.now().timetuple()))
        emailText = \
            u"From: Mensajero (no responder) <contacto@daem.cl>\n" \
            u"To: " + purchaseOrder.emailRecipients() + "\n" \
            u"Subject: Orden de Compra\n" \
            u"Content-Type: text/plain; charset=UTF-8;\n" \
            u"Content-Transfer-Encoding: 8bit\n" \
            u"Date: " + emailDate + "\n\n" \
            u"Nueva Orden de compra\n"
        if purchaseOrder.classification != None and purchaseOrder.classification != '':
            emailText += u"Clasificación: " + purchaseOrder.classification + "\n\n"
        emailText += u"Items solicitados\n\n"
        for item in purchaseOrder.items:
            emailText += " - " + item + "\n"
        emailText += \
            u"\nEsta OC también se ha guardado en:\n" + purchaseOrder.absolute_url() + "\n\n"
        logger.info("Email")
        logger.info(emailText)
    
        try:
            host = getToolByName(purchaseOrder, 'MailHost')
            # The ``immediate`` parameter causes an email to be sent immediately
            # (if any error is raised) rather than sent at the transaction
            # boundary or queued for later delivery.
            return host.send(emailText, immediate=True, charset='utf-8')
        except SMTPRecipientsRefused:
            # Don't disclose email address on failure
            raise SMTPRecipientsRefused('Recipient address rejected by server')
