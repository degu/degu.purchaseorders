degu.pruchaseorders
===================

Introduction
------------

This product include content types to recive purchase orders and sent
email notifications to users. This product provides two content types:

- *Purchase Order Folder* allows to send and store purchase orders.
- *Purchase Order* contains the description of a purchase order.


Installing it
-------------

Adds this line to your buildout (degu.purchaseorders).





